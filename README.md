# README #

Dette repository indeholder TodoApi projektet, som er et simpelt CRUD WebApi Core projekt (master branch)
Desuden er der tilføjet Swagger API-dokumentation (swagger branch)

### What is this repository for? ###

* Building Your First Web API with ASP.NET Core MVC and Visual Studio[Link Text](https://docs.asp.net/en/latest/tutorials/first-web-api.html)
* [ASP.NET Web API Help Pages using Swagger](https://docs.asp.net/en/latest/tutorials/web-api-help-pages-using-swagger.html)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact